from ase.io import read

atoms = read("structures/Pd_on_Pt-111-4_4_4-Square.POSCAR")

from ase.calculators.emt import EMT
atoms.set_calculator(EMT())

from ase.optimize import BFGS

opt = BFGS(atoms, trajectory="trajectories/Pd_on_Pt.traj")
opt.run(fmax=0.01)
