from ase.io import read
from ase.calculators.vasp import Vasp

atoms = read("structures/Pd_on_Pt-111-4_4_4-Square.POSCAR")

vasp_std = "srun --mpi=pmi2 vasp_std"
vasp_gam = "srun --mpi=pmi2 vasp_gam"
calc = Vasp(command=vasp_gam,
            directory="vasp_opt",
            txt="-",
            xc="pbe",
            encut=300,
            enaug=300 * 1.5,
            ediff=1e-5,
            ismear=0,
            sigma=0.2,
            gamma=True,
            isif=2,
            ibrion=2,
            ediffg=-0.02,
            lcharg=False,
            lwave=False,
            nsw=100,
            kpts=(1, 1, 1))

atoms.set_calculator(calc)
atoms.get_potential_energy()
