from ase.build import bulk

# Pt prim
atoms = bulk("Pt")
atoms.write("structures/Pt-prim.POSCAR")

# Pt conventional (cubic)
atoms = bulk("Pt", cubic=True)
atoms.write("structures/Pt-cubic.POSCAR")

# Pt3Cu
atoms = bulk("Pt", cubic=True)
atoms.symbols[-1] = "Cu"
atoms.write("structures/Pt3Cu.POSCAR")

# Pt3Cu Supercell
atoms *= (3, 2, 2)
atoms.write("structures/Pt3Cu-X2.POSCAR")

# Pd3M Alloys
for alloy_element in ["Pt", "Sn", "Zn", "Th", "Ca", "Li", "Cu", "Ni", "C"]:
    atoms = bulk("Pd", cubic=True)
    atoms.symbols[-1] = alloy_element
    atoms.write(f"structures/Pd3{alloy_element}.POSCAR") # This is an f-string!
