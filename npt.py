from ase.io import read
from ase.calculators.emt import EMT
from ase import units

atoms = read("structures/Pt3Cu-X2.POSCAR")
atoms.set_calculator(EMT())

from ase.md.velocitydistribution import MaxwellBoltzmannDistribution
from ase.md.npt import NPT

MaxwellBoltzmannDistribution(atoms, temperature_K=300)

dyn = NPT(atoms, timestep=5 * units.fs, temperature_K=300,
                   ttime=25 * units.fs, externalstress=0,
                   pfactor=(50 * units.fs)**2,
                   trajectory="trajectories/Pd3Cu-MD-300K-NPT.traj")

def printenergy(a=atoms):
    """Function to print the potential, kinetic and total energy"""
    epot = a.get_potential_energy() / len(a)
    ekin = a.get_kinetic_energy() / len(a)
    print('Energy per atom: Epot = %.3feV  Ekin = %.3feV (T=%3.0fK)  '
          'Etot = %.3feV' % (epot, ekin, ekin / (1.5 * units.kB), epot + ekin))

# Now run the dynamics
printenergy(atoms)
dyn.attach(printenergy)
dyn.run(1000)
