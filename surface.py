from ase.build import fcc111
from ase.build import surface
from ase.build import fcc111_root
from ase.constraints import FixAtoms

# Make a simple Pt(111) surface

atoms = fcc111("Pt", size=(4, 4, 4), a=3.92, vacuum=5)
atoms.set_constraint(FixAtoms(mask=[atom.tag > 2 for atom in atoms]))

atoms.write("structures/Pt-111-4_4_4.POSCAR")

## Square cell

atoms = fcc111("Pt", size=(4, 4, 4), a=3.92, vacuum=5, orthogonal=True)
atoms.set_constraint(FixAtoms(mask=[atom.tag > 2 for atom in atoms]))

atoms.write("structures/Pt-111-4_4_4-Square.POSCAR")

# Root cell

atoms = fcc111_root("Pt", 12, (1, 1, 4), a=3.92, vacuum=5)
atoms.set_constraint(FixAtoms(mask=[atom.tag > 2 for atom in atoms]))

atoms.write("structures/Pt-111-r12.POSCAR")

# Surface Alloy

atoms = fcc111("Pt", size=(4, 4, 4), a=3.92, vacuum=5, orthogonal=True)
atoms.set_constraint(FixAtoms(mask=[atom.tag > 2 for atom in atoms]))
atoms.symbols = ["Pd" if atom.tag == 1 else "Pt" for atom in atoms]

atoms.write("structures/Pd_on_Pt-111-4_4_4-Square.POSCAR")

