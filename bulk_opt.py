from ase.io import read

atoms = read("structures/Pd3C.POSCAR")

from ase.calculators.emt import EMT
atoms.set_calculator(EMT())

from ase.optimize import BFGS
from ase.constraints import ExpCellFilter

cell_filter = ExpCellFilter(atoms)
opt = BFGS(cell_filter, trajectory="trajectories/Pd3C.traj")
opt.run(fmax=0.01)
