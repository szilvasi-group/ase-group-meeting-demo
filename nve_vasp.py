from ase.io import read
from ase.calculators.vasp import Vasp
from ase import units

atoms = read("structures/Pt3Cu.POSCAR")

#####################################

vasp_std = "srun --mpi=pmi2 vasp_std"
vasp_gam = "srun --mpi=pmi2 vasp_gam"
calc = Vasp(command=vasp_std,
            directory="vasp_nve",
            txt="-",
            xc="pbe",
            encut=400,
            enaug=400 * 1.5,
            ediff=1e-6,
            ismear=0,
            sigma=0.2,
            gamma=True,
            lcharg=False,
            lwave=False,
            kpts=(2, 2, 2))

#####################################


atoms.set_calculator(calc)

from ase.md.velocitydistribution import MaxwellBoltzmannDistribution
from ase.md.verlet import VelocityVerlet

MaxwellBoltzmannDistribution(atoms, temperature_K=300)

# We want to run MD with constant energy using the VelocityVerlet algorithm.
dyn = VelocityVerlet(atoms, 5 * units.fs, trajectory="trajectories/Pd3Cu-MD-300K-NVE.traj")  # 5 fs time step.

def printenergy(a=atoms):
    """Function to print the potential, kinetic and total energy"""
    epot = a.get_potential_energy() / len(a)
    ekin = a.get_kinetic_energy() / len(a)
    print('Energy per atom: Epot = %.3feV  Ekin = %.3feV (T=%3.0fK)  '
          'Etot = %.3feV' % (epot, ekin, ekin / (1.5 * units.kB), epot + ekin))

# Now run the dynamics
printenergy(atoms)
dyn.attach(printenergy)
dyn.run(1000)
